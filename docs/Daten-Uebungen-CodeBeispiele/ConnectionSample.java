import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;


public class ConnectionSample {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {


		//word we want to search for:
    	String word = "apple";
    	
    	//call web-service and get input stream result:
        URL url = new URL("http://services.aonaware.com/" +
        		"DictService/DictService.asmx/Define?word="+ word);
        URLConnection yc = url.openConnection();
        
        //test and show result as String:
        BufferedReader in = new BufferedReader
        (new InputStreamReader(yc.getInputStream()));
        String inputLine;
        while ((inputLine = in.readLine()) != null) 
            System.out.println(inputLine);
        in.close();

	}

}
