### Aufgabe/Task: Nr. 09

Thema: HashMaps in Java

Geschätzter Zeitbedarf: 60-120 min

#### Aufgabenbeschreibung:

Schauen Sie sich zuerst diese Folin zum Einstieg an
 [HashMapUndRekursion.pptx](./HashMapUndRekursion.pptx) / [HashMapUndRekursion.pdf](./HashMapUndRekursion.pdf)


Und dann schauen Sie ein/zwei der angebotenen Videos über Java HashMap an
- [Videos-Tutorials-Anleitungen](../docs/Videos-Tutorials-Anleitungen)
  - <https://bscw.tbz.ch/bscw/bscw.cgi/32213665>  

Eine weitere Anleitung zu
 - HashMap Java Tutorial (12 min) <https://www.youtube.com/watch?v=70qy6_gw1Hc>


.. und bauen Sie in Java eine eigene HashMap nach und zeigen Sie es der Lehrperson (evtl. Abgabe).


Bewertung: Keine, ist aber prüfungsrelevant
