# M411 - Datenstrukturen und Algorithmen entwerfen und anwenden

[> **Modulidentifikation** ](https://www.modulbaukasten.ch/modul/bc75c9da-716c-eb11-b0b1-000d3a830b2b)

- [docs](./docs/)
- [docs/Videos-Tutorials-Anleitungen](./docs/Videos-Tutorials-Anleitungen)

## Aufträge & Übungen
| Tag  | Titel                          | Auftrag, Übung, Themen |
| ---- | ------                         | --------------         |
|   1  | [First steps](./1)             | Modulvorstellung <br>- what is an algorithm <br>- what are data structures <br>-first steps I (classes, main-methode)|
|   2  | [file handling](./2)                  | first steps II / reading user input (scanner) / reading and writing files (BufferedReader)  <br>working with arrays <br> - fill, search, update, delete array elements <br> - sorting (BubbleSort) |
|   3  | [Linked Lists](./3)            | follow up arrays <br> linked lists (self made) |
|   4  | [Sort, Stack, Queue](./4)      | compare sort-algorithms <br> - BubbleSort vs QuickSort <br> - BubbleSort vs ??Sort <br> incl. mesurement the speed <br> - stack (push, pop) and queues (FIFO, LIFO, LILO, FILO) |
|   5  | [Hashmaps, Recursions](./5)    | - hash maps, <br>- recursions , back tracking  |
|   6  | **LB1** (30% MN) <br>- 30 min on paper and<br>- 60 min coding <br> themes: arrays, stacks, sorts, linked lists |  - follow up [hash maps, recursions](./5) <br>- [collections](./6)                      |
|   7  | **LB2** (30% MN) <br>- 30 min on paper and<br>- 60 min coding <br> themes: hash maps and rekursions | after LB2 start mini project (LB3),<br>find a team partner and find a [problem to solve]((./docs/Unterlagen_LB3_MiniProjekte))<br>- [trees and graphs](./7) <br>- [Dijkstra-Algorithm (route planner)](./7)  <br>- [xml data, json data](./7) <br>- [page rank algorithm](./7) |
|   8  | **LB3** (40% MN) <br> start mini project <br> team of 2 persons | - search and [decide a  mini-projekt](./docs/Unterlagen_LB3_MiniProjekte) <br>- work on mini project |
|   9  | work on mini project           |                        |
|  10  | last work and<br>close mini project    | **Note 4.0** = "genügend"<br>(d.h. es ist alles da und funktioniert irgendwie, niedrige Komplexität) <br> **Note 5.0** = "gut"<br>(d.h. gut gelöst und funktioniert einwandfrei, adäquate Komplexität) <br> **Note 6.0** = "sehr gut" <br>(übertrifft die Erwartungen, hohe Komplexität) |
