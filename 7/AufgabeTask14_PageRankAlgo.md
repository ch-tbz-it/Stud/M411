### Aufgabe/Task: Nr. 15

Thema: Page Ranking Algorithmus  
(googles patent algorithm)

Geschätzter Zeitbedarf: 90-120 min

Aufgabenbeschreibung:

Schauen Sie sich 2-3 dieser Videos auf der Liste an und beschreiben Sie
anschliessend im Detail mit eigenen Worten, wie der Algorithmus funktioniert.
Erwartet wird etwa eine A4-Seite (in 11 Pt Schrift)

<https://bscw.tbz.ch/bscw/bscw.cgi/31933102>



Bewertung: Keine
