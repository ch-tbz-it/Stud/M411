Aufgabe/Task: Nr. 13

Thema: Data-Files-Structures, XML, JSON


Geschätzter Zeitbedarf: 120 min

Aufgabenbeschreibung:
Bilden Sie sich über den Java-Syntax bezüglich xml und json und probieren Sie es aus.
Gehen Sie dem Dokument [script6_weitereDatenstrukturen_XML_JSON.pdf](./script6_weitereDatenstrukturen_XML_JSON.pdf) nach.


Bewertung: Keine.
