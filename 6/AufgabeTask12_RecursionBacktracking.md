### Aufgabe/Task: Nr. 10

Thema: Recursion and Backtracking

Geschätzter Zeitbedarf: 120-150 min

#### Aufgabenbeschreibung:

Betrachten Sie zuerst diese Folien
- [HashMapUndRekursion.pptx](./HashMapUndRekursion.pptx) / [HashMapUndRekursion.pdf](./HashMapUndRekursion.pdf)

Danach studieren Sie folgendes Dokument, indem Sie alles nachbauen, was drin vorkommt.
- [script7_rekursionUndBacktracking.pdf](./script7_rekursionUndBacktracking.pdf)

Versuchen Sie insbesondere die Iteration in eine Rekursion umzubauen.
- [script7c_vonIterationZuRekursion.pdf](./script7c_vonIterationZuRekursion.pdf)


Weitere Videos zum Thema:
- [Videos-Tutorials-Anleitungen](../docs/Videos-Tutorials-Anleitungen)

Benützen sie vielleicht auch folgende Code-Stücke:
- [Rekursion: DiskUsage.java](../docs/Daten-Uebungen-CodeBeispiele/Rekursion/DiskUsage.java) 
- [Backtracking: SudokoSolver.java](../docs/Daten-Uebungen-CodeBeispiele/Backtracking(Sudoku)/SudokoSolver.java)



Bewertung: Keine, ist aber prüfungsrelevant
