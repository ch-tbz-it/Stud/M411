### Aufgabe/Task: Nr. 01

Thema: Was ist ein Algorithmus?

Geschätzter Zeitbedarf: 40-60min

#### Aufgabenbeschreibung:

1.) Schauen Sie sich diese 3 verlinkten Videos.
 - 3:49 min, D, 2019: Algorithmen in 3 Minuten erklärt <https://www.youtube.com/watch?v=FBUoEumkP2w>
 - 8:43 min, D, 2020: Was ist ein Algorithmus? <https://www.youtube.com/watch?v=GoIACw9ARCM>
 - 4:24 min, D, 2018: Was sind Algorithmen <https://www.youtube.com/watch?v=Z0WvGofejVg>


2.) Schreiben Sie mit eigenen Worten\*\*\* und geben Sie das Resultat in einem
PDF im TEAMS ab,
 - a.) Was ist ein Algorithmus?
 - b.) Wo kann man Algorithmen einsetzen?
 - c.) Was sind die Eigenschaften eines Algorithmus?


3.) Diskutieren und lösen Sie die *beiden* Fragen in der **2 Praktische Übung** im
verlinkten Skript-Dokument:
- [script1_0_Einführung.pdf](./script1_0_Einführung.pdf)
<br> <br> Wenn Sie noch Zeit haben, lösen Sie noch den **2 Practical Task** im englischen Dokument:
- [script1_0_introduction.pdf](./script1_0_introduction.pdf)

Sie dürfen auch mit Klassenkameraden darüber diskutieren. Aber jede/r schreibt
seinen eigenen Text.

*\*\*\* immer ganze Sätze schreiben. In De oder in En, wie Sie wollen. Nicht nur
Stichworte, Erklärungen sind gefragt.*



Bewertung: Es gibt Punkte, die sind aber nicht modulnotenrelevant. Sie machen aber eine
Aussage, wie gut die Lehrperson Ihr Text findet.
